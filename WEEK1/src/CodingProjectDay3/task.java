package CodingProjectDay3;

import java.util.ArrayList;
import java.util.Scanner;

public class  task {
    private int taskId;
    private String taskName;
    private String taskText;
    private String assignedTo;
    public task()
    {

    }
    Scanner sc =new Scanner(System.in);
    public int getId()
    {
        return this.taskId;
    }
    public void setId(int i)
    {
        this.taskId=i;
    }
    public String getTaskTitle() {
		return this.taskName;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskName = taskTitle;
	}

	public String getTaskText() {
		return this.taskText;
	}

	public void setTaskText(String taskText) {
		this.taskText = taskText;
	}

	public String getAssignedTo() {
		return this.assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
    public task(int taskId, String taskTitle, String taskText, String assignedTo) {
		this.taskId = taskId;
		this.taskName = taskTitle;
		this.taskText = taskText;
		this.assignedTo = assignedTo;
	}
    void viewTask(ArrayList<task> arrTasks){
        for (int a=0;a<arrTasks.size();a++) {
            System.out.println("Task num "+(a+1) +": "+arrTasks.get(a).taskName);
        }
    }
    public void addTask(ArrayList<task> arrTasks) {
		System.out.println("Enter the number of Tasks you want to add: ");
		int t = Integer.parseInt(sc.nextLine());
		for (int i = 0; i < t; i++) {
			int id = arrTasks.size();
			System.out.println("Task "+(id+1)+": ");
			System.out.print("Enter the Task Title: ");
			String taskTitle = sc.next();
			System.out.print("Enter the Task Text: ");
			String taskText = sc.next();
			System.out.print("Enter the assigned: ");
			String assignedTo = sc.next();
			task tasks = new task(id, taskTitle, taskText, assignedTo);
			arrTasks.add(tasks);
		}
	}
    public void updateTask(ArrayList<task> arrTasks) {
        viewTask(arrTasks);
        System.out.println("Enter the task index you want to update: ");
		int index = sc.nextInt();
        System.out.print("Enter the Task Title: ");
        arrTasks.get(index+1).setTaskTitle(sc.next());
        System.out.print("Enter the Task Text: ");
        arrTasks.get(index+1).setTaskText(sc.next());
        System.out.print("Enter the assigned: ");
        arrTasks.get(index+1).setAssignedTo(sc.next());
        System.out.println("Update Successful");


    }
    public void searchTask(ArrayList<task> arrTasks) {
        System.out.println("Enter the keyword you want to search: ");
		String search = sc.nextLine();
        System.out.println("Result :");
		for (int i = 0; i < arrTasks.size(); i++) {
			if(arrTasks.get(i).taskText.toLowerCase().trim().contains(search.toLowerCase().trim())) {
                
				System.out.println(arrTasks.get(i).taskName);
			}
		}
		
    }
    public void assigneeTask(ArrayList<task> arrTasks) {
        viewTask(arrTasks);
        System.out.println("Enter the task index you want to assignee: ");
		int index = sc.nextInt();
        System.out.print("Enter new task Own: ");
        arrTasks.get(index+1).setTaskTitle(sc.next());
      
    }

}
