package CodingProjectDay5;

import java.util.*;


public class Client {

	public  TaskDAOImp taskImp = new TaskDAOImp();
	public void MenuClient(ArrayList<Task> arrTask) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Menu");
		System.out.println("1. Add ");
		System.out.println("2. Update ");
		System.out.println("3. Delete ");
		System.out.println("4. Search ");
		System.out.println("5. Display ");
		System.out.println("6. Sort Arr Task ");
		System.out.println("7. Sign out ");
		System.out.println("0. Exit ");
		System.out.println("Enter your choice: ");
		int ch = Integer.parseInt(sc.nextLine());
		switch (ch) {
		case 0:
			break;
		case 1:
			taskImp.Input_Arr(arrTask);
			MenuClient(arrTask);
			break;
		case 2:
			taskImp.Update_Task(arrTask);
			MenuClient(arrTask);
			break;
		case 3:
			taskImp.Delete_Task(arrTask);
			MenuClient(arrTask);
			break;
		case 4:
			taskImp.Search_by_TaskTitle(arrTask);
			MenuClient(arrTask);
			break;
		case 5:
			taskImp.DisplayAllTask(arrTask);
			MenuClient(arrTask);
			break;
		case 6:
			taskImp.SortArrTask(arrTask);
			MenuClient(arrTask);
			break;
		case 7:
			Main ngay5 = new Main();
			ngay5.Login(arrTask);
			break;
		default:
			Main d5 = new Main();
			System.out.println("Incorrect input format!");
			d5.Login(arrTask);
			break;
		}
	}



}
