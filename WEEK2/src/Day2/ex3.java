package Day2;

import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;

public class ex3 {
	 Scanner sc=new Scanner(System.in);
	    public void addElemnt(TreeSet<String> arr) {
			System.out.println("Enter the number of element you want to add: ");
			int t = Integer.parseInt(sc.nextLine());
			for (int i = 0; i < t; i++) {
				int id = arr.size();
				System.out.println("Element "+(id+1)+": ");
				System.out.print("Enter the element value: ");
				String val = sc.next();
	            if(!arr.add(val))
	            {   i-=1;
	                System.out.println("Element already exit");
	            }
				
			}
		}
	    public void viewAll(TreeSet<String> ts) {
	        int count=0;
	        for (String integer : ts) {
	            count++;
	            System.out.println("Element num "+count +": "+integer);
	            
	        }
	    }
	    public void viewFirst(TreeSet<String> ts) {
	        System.out.println("First : "+ts.first());


	    }
	    public void viewLast(TreeSet<String> ts) {
	        System.out.println("Last : "+ts.last());

	    }
	    public void RemoveLast(TreeSet<String> ts) {
	        ts.pollLast();
	    }
	    public void getSize(TreeSet<String> ts) {
	        System.out.println("Size : "+ts.size());
	    }
	    public void viewByIteratorInterface(TreeSet<String> ts) {
	        Iterator<String> value = ts.iterator();
	  
	        System.out.println("The string values are: ");
	        while (value.hasNext()) {
	            System.out.println(value.next());
	        }
	    }
	    static TreeSet<String> ts = new TreeSet<>(); 
	    static void menu()
	    {
	     
	        Scanner sc = new Scanner(System.in);
	        System.out.println("Choose option :");
	        System.out.println("1.Add element:");
	        System.out.println("2.View all element :");
	        System.out.println("3.View first element:");
	        System.out.println("4.View last element:");
	        System.out.println("5.Remove last element:");
	        System.out.println("6.Get size:");
	        System.out.println("7.View all element by iterator interface:");

	        System.out.println("0.Exit app :");
	        String key = sc.next();
	        ex3 t = new ex3();
	        switch (key) {
	            case "1":
	             
	                t.addElemnt(ts);
	                menu();
	                break;
	            case "2":
	                t.viewAll(ts);
	                menu();
	                break;
	            case "3":
	                t.viewFirst(ts);
	                menu();
	                break;
	            case "4":
	                t.viewLast(ts);
	                menu();
	                break;
	        
	            case "5":
	                t.RemoveLast(ts);
	                menu();
	                break;
	            case "6":
	                t.getSize(ts);
	                menu();
	                break;

	            case "7":
	                t.viewByIteratorInterface(ts);
	                menu();
	                break;
	            case "0":
	                System.exit(0);
	                menu();
	                break;
	            default: 
	                menu();
	                break;
	        }
	        
	        sc.close();

	    }
	    public static void main(String[] args) {
	        menu();
	    }

}
