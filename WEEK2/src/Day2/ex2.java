package Day2;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

public class ex2 {
	Scanner sc=new Scanner(System.in);

    public void addElemnt(Deque<Integer> arr) {
		System.out.println("Enter the number of element you want to add: ");
		int t = Integer.parseInt(sc.nextLine());
		for (int i = 0; i < t; i++) {
			int id = arr.size();
			System.out.println("Element "+(id+1)+": ");
			System.out.print("Enter the element value: ");
			int val = sc.nextInt();
		

			arr.add(val);
		}
	}

    public void addToHead(Deque<Integer> deque) {
        System.out.println("Add to Head \n Enter element value :");
        int k=sc.nextInt();
        deque.addFirst(k);
    }
    public void addToTail(Deque<Integer> deque) {
        System.out.println("Add to Tail \n Enter element value :");
        int k=sc.nextInt();
        deque.addLast(k);
    }
    public void viewAll(Deque<Integer> s)
    {
        int count=0;
        for (Integer integer : s) {
            count++;
            System.out.println("Element num "+count +": "+integer);
            
        }
    }

    public void viewFirst(Deque<Integer> deque) {
        
        System.out.println("the first element "+deque.peek());
    }

    public void clear(Deque<Integer> deque) {
        deque.clear();
        System.out.println("Done ! ");
    }
    static Deque<Integer> deque = new ArrayDeque<Integer>();
    static void menu()
    {
     
        Scanner sc = new Scanner(System.in);
        System.out.println("Choose option :");
        System.out.println("1.Add element:");
        System.out.println("2.Insert to head:");
        System.out.println("3.Insert to tail:");
        System.out.println("4.View all element :");
        System.out.println("5.View first element:");
        System.out.println("6.Delete all :");
        System.out.println("0.Exit app :");
        String key = sc.next();
        ex2 t = new ex2();
        switch (key) {
            case "1":
             
                t.addElemnt(deque);
                menu();
                break;
            case "2":
             
                t.addToHead(deque);
                menu();
                break;
            case "3":
                t.addToTail(deque);
                menu();
                break;
            case "4":
                t.viewAll(deque);
                menu();
                break;
        
            case "5":
                t.viewFirst(deque);
                menu();
                break;
            case "6":
                t.clear(deque);
                menu();
                break;
          
            case "0":
                System.exit(0);
                menu();
                break;
            default: 
                menu();
                break;
        }
        
        sc.close();

    }
    public static void main(String[] args) {
        menu();
    }

}
