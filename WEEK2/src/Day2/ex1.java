package Day2;

import java.util.PriorityQueue;
import java.util.Scanner;

public class ex1 {
	Scanner sc=new Scanner(System.in);
    public void addElemnt(PriorityQueue<Integer> arr) {
		System.out.println("Enter the number of element you want to add: ");
		int t = Integer.parseInt(sc.nextLine());
		for (int i = 0; i < t; i++) {
			int id = arr.size();
			System.out.println("Element "+(id+1)+": ");
			System.out.print("Enter the element value: ");
			int val = sc.nextInt();
		

			arr.add(val);
		}
	}
    public void printTop(PriorityQueue<Integer> arr)
    {
        System.out.println("Top element : "+arr.peek());
    }
    public void printSize(PriorityQueue<Integer> v)
    {
        System.out.println("size :" +v.size());
    }
    public void deleteOne(PriorityQueue<Integer> pQueue) {
        System.out.println("Enter value you want to remove :");
        int k= sc.nextInt();
        if(pQueue.remove(k))    
        {
            System.out.println("done");
        }
        else
            System.out.println("not found");
    }
    public void viewAll(PriorityQueue<Integer> pQueue) {
        System.out.println(pQueue);
    }
    public void checkNull(PriorityQueue<Integer> pQueue) {
        System.out.println("Null :"+pQueue.isEmpty());
    }
    static   PriorityQueue<Integer> pQueue = new PriorityQueue<Integer>();
    static void menu()
    {
     
        Scanner sc = new Scanner(System.in);
        System.out.println("Choose option :");
        System.out.println("1.Add element:");
        System.out.println("2.Print the top elemet:");
        System.out.println("3.Print size:");
        System.out.println("4.Print the head :");
        System.out.println("5.Delete an element:");
        System.out.println("6.View all :");
        System.out.println("7.Check if null :");
        System.out.println("0.Exit app :");
        String key = sc.next();
        ex1 t = new ex1();
        switch (key) {
            case "1":
                t.addElemnt(pQueue);
                menu();
                break;
            case "2":
                t.printTop(pQueue);
                menu();
                break;
            case "3":
                 t.printSize(pQueue);
                menu();
                break;
            case "4":
                t.printTop(pQueue);
                menu();
                break;
        
            case "5":
                t.deleteOne(pQueue);
                menu();
                break;
            case "6":
                t.viewAll(pQueue);
                menu();
                break;
            case "7":
                t.checkNull(pQueue);
                menu();
                break;
            case "0":
                System.exit(0);
                menu();
                break;
            default: 
                menu();
                break;
        }
        
        sc.close();

    }
    public static void main(String[] args) {
        menu();
    }

}
