package week2final;
import java.util.*;
import java.util.Iterator;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class UserImp {
	MagicOfBooks magicOfBooks = new MagicOfBooks();
	Scanner scanner = new Scanner(System.in);

	Main week2 = new Main();

	public void Menu(ArrayList<BookAttributes> arrBook, ArrayList<User> arrUser, String Username) {
		System.out.println("Menu");
		System.out.println("1. Display list book");
		System.out.println("2. Display my favorite book ");
		System.out.println("3. Search book by id ");
		System.out.println("4. Sign out ");
		System.out.println("0. Exit ");
		System.out.print("Enter your choice: ");
		int ch = Integer.parseInt(scanner.nextLine());
		System.out.println();
		switch (ch) {
		case 0:
			System.out.println("Stop programing!!!");
			System.exit(0);
			break;
		case 1:
			DisplayListBook(arrBook);
			Menu(arrBook, arrUser, Username);
			break;
		case 2:
			DisplayFavoriteBook(arrBook, arrUser, Username);
			Menu(arrBook, arrUser, Username);
			break;
		case 3:
			SearchBookbyID(arrBook, Username, arrUser);
			Menu(arrBook, arrUser, Username);
			break;
		case 4:
			week2.Login(arrUser, arrBook);
			break;
		default:
			UserImp tuan2 = new UserImp();
			System.out.println("Please enter int !");
			week2.Login(arrUser, arrBook);
			break;
		}
	}
	public void LogFile(String logMessenger) {
		Logger logger = Logger.getLogger("Log");
		FileHandler fh = null;
		logger.setUseParentHandlers(false);
		try {
			fh = new FileHandler("log.log", true);
			logger.addHandler(fh);
		} catch (Exception e) {
			e.printStackTrace();
		}
		SimpleFormatter formatter = new SimpleFormatter();
		fh.setFormatter(formatter);

		logger.info(logMessenger);

	}

	public void DisplayListBook(ArrayList<BookAttributes> arrBook) {

		for (BookAttributes book : arrBook) {
			System.out.println(book.toString());
		}
	}

	public void DisplayFavoriteBook(ArrayList<BookAttributes> arrBook, ArrayList<User> arrUser, String username) {
		for (User user : arrUser) {
			if (user.getUserName().trim().equals(username)) {
				for (int i : user.getFavourite()) {
					for (int j = 0; j < arrBook.size(); j++) {
						if (arrBook.get(j).getBookId() == i) {
							System.out.println(arrBook.get(j).toString());
						}
					}
				}
			}
		}
	}

	public void SearchBookbyID(ArrayList<BookAttributes> arrBook, String username, ArrayList<User> arrUser) {
		System.out.print("Enter ID_book you want to search: ");
		while (!scanner.hasNextInt())
		{
			scanner.next();
			System.err.print("That wasn't an int number. Try again: \n");
		}
		int id = scanner.nextInt();
		System.out.println();
		magicOfBooks.SearchbyID(arrBook, id);
		System.out.print("Do you want to see detail the book? Enter 'Y' to see detail -> ");
		String tmp = scanner.nextLine();
		if (tmp.trim().toUpperCase().equals("Y")) {
			DisplayBookByID(arrBook, id);
		}
		System.out.println();
		System.out.print("Do you like it? Enter 'Y' to add book in list favorite book ");
		String tmpString = scanner.nextLine();
		if (tmpString.trim().toUpperCase().equals("Y")) {
			for (User user : arrUser) {
				if (user.getUserName().trim().equals(username)) {
					user.getFavourite().add(id);
					System.out.println("Add favourite successful!");
				}
			}
			
		}
	}

	public void DisplayBookByID(ArrayList<BookAttributes> arrBook, int id) {
		MagicOfBooks magic = new MagicOfBooks();
		magic.DisplayBookbyID(arrBook, id);
		DisplayBookByID(arrBook, id);
	}

}
